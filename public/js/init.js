(function($){
  $(function(){

    $('.button-collapse').sideNav();

    $('.parallax').parallax();

    $('select').material_select();

    $('#attachment').modal();

    $('.datepicker').pickadate({
      selectMonths: true,
      selectYears: 15,
      today: 'Today',
      clear: 'Clear',
      close: 'Ok',
      closeOnSelect: false
    });

    $('.timepicker').pickatime({
      default: 'now',
      fromnow: 0,
      twelvehour: false,
      donetext: 'OK',
      cleartext: 'Clear',
      canceltext: 'Cancel',
      autoclose: false,
      ampmclickable: true,
      aftershow: function () { }  
    });

    $(".delete-bxtn").click(function() {
      swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this imaginary file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {
            swal("Poof! Your imaginary file has been deleted!", {
              icon: "success",
            });
          } else {
            swal("Your imaginary file is safe!");
          }
        });
    });
    
    $('#submit-report').click(function(e) {
      e.preventDefault();
      swal("Success!", "You have saved your report!", "success");
    });

    $('#save-plate').click(function(e) {
      e.preventDefault();
      swal("Success!", "You have succesfully saved your plate.", "success")
    });

    $('#save-hugot').click(function (e) {
      e.preventDefault();
      swal("Success!", "You have succesfully saved your hugot.", "success")
    });

    $('#save-user').click(function (e) {
      e.preventDefault();
      swal("Success!", "You have succesfully saved your user.", "success")
    });
    
  });
})(jQuery);