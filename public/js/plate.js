(function($){
    $(function(){
        $(".delete-btn").click(function () {
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this record!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
              .then((willDelete) => {
                if (willDelete) {
                  var anchorClicked = $(this)[0].attributes[1].value;
                  console.log($(this)[0].attributes[1].value);
                 
                  deleteUrl = APP_URL+"/delete-report/"+anchorClicked;
      
                  $.ajax({
                    type: 'GET',
                    url: deleteUrl,
                    success: function (data) {
                      swal("Record deleted!", {
                        icon: "success",
                        buttons: false
                      })
                      setTimeout(function(){ location.reload() }, 1000);
                    },
                    error: function (data) {
                      console.log(data);
                      swal("NOT Deleted!", "Something blew up.", "error");
                    }
                  });
      
                } else {
                  // swal("Your imaginary file is safe!");
                }
              });
          });
    });
  })(jQuery);