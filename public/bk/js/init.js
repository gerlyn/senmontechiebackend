(function($){
  $(function(){

    $('.button-collapse').sideNav();

    $('.parallax').parallax();

    $('select').material_select();

    $('.datepicker').pickadate({
      selectMonths: true,
      selectYears: 15,
      today: 'Today',
      clear: 'Clear',
      close: 'Ok',
      closeOnSelect: false
    });

    $('.timepicker').pickatime({
      default: 'now',
      fromnow: 0,
      twelvehour: false,
      donetext: 'OK',
      cleartext: 'Clear',
      canceltext: 'Cancel',
      autoclose: false,
      ampmclickable: true,
      aftershow: function () { }  
    });
    
    $('#attachment').modal();

    var $report_plate_number = $('input[name=report_plate_number]');
    $report_plate_number.blur(function(){
      // alert($(this).val());
        $.ajax({
          type: 'GET',
          url: APP_URL+"/findPlateNumber?plate_number="+$(this).val(),
          success: function (data) {
            if(data.message.length > 0){
              $report_plate_number.val('');
              swal("Not found or not registered yet.", {
                icon: "info",
                buttons: false
              })
            }
          },
          error: function (data) {
          }
        });
    });


  });
})(jQuery);