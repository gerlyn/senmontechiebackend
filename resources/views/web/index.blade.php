@include('web.head')
@include('web.nav')



    <div class="container">
        <div class="section check-license">
            <div class="row">
                <div class="col s12">
                    <h1 class="text-center">Check your plate number!</h1>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m2 push-m2">
                    <img src="../../img/highway-patatas.png" class="responsive-img">
                </div>
                <div class="col s12 m6 push-m2">
                    <form action="{{url('')}}" method="POST">
                    {{csrf_field()}}
                        <div class="input-field">
                            <input id="plate_number" name="plate_number" type="text" class="validate" value="{{old('plate_number')}}">
                            <label for="plate_number">Enter Plate No.</label>
                            <span class="{{ $errors->has('plate_number') ? 'required' : 'hide' }}" style="color:red;">*Required</span>
                        </div>

                        <button class="btn btn-large waves-effect waves-light teal accent-4" type="submit" name="action">Check
                            <i class="material-icons right">check</i>
                        </button>
                    </form>
                </div>
                <div class="col s12 text-center">
                    @if(count($data) != 0)
                        <!-- @if(count(json_decode($data)->message[0]) > 0) -->
                            <h1>{{json_decode($data)->message}}</h1>
                        <!-- @endif -->
                    @endif
                </div>
            </div>
        </div>        
    </div>

    <div class="pastel-orange">
        <div class="container">
            <div class="section">
                <h2 class="text-center">Who will be using our App?</h2>
                <div class="row">
                    <div class="col s12 m4 text-center">
                        <img src="../../img/motorist.png" title="Motorist">
                        <h3>Motorist</h3>
                    </div>
                    <div class="col s12 m4 text-center">
                        <img src="../../img/commuter.png" title="Commuter">
                        <h3>Commuter</h3>
                    </div>
                    <div class="col s12 m4 text-center">
                        <img src="../../img/mmda.png" title="MMDA">
                        <h3>MMDA</h3>
                    </div>
                </div>
            </div>
        </div>        
    </div>

    <div class="container">
        <div class="section">
            <div class="row scope">
                <div class="col s12 text-center">
                    <img src="../../img/scope.png" title="Scope">
                    <h2>Scope</h2>
                    <ul>
                        <li>Road Issues</li>
                        <li>Traffic Violations  </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container"><div class="divider"></div></div>

    <div class="container future-dev">
        <div class="section text-center">
            <img src="../../img/future_development.png" title="Future Development">
            <h2 class="text-center">Future Development and Recommendations</h2>
            <div class="row">
                <div class="col s12">
                    <ul >
                        <li>Security/Authentication - users can sync Facebook Account for faster gathering of information</li>
                        <li>This App can be a good recommendation to MMDA.</li>
                        <li>Reported accidents/complaints can be updated by the users.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@include('web.footer')

</body>

</html>