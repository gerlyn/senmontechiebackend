
@include('web.head')
@extends('web.script-extend')
@section('import-page-js', 'hugot.js')

@include('web.nav')
<form action="{{url('delete-report')}}" method="GET" id="formDeleteReport">
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12">

                    <a class="btn waves-effect waves-light pull-right light-blue m-t-25" href="#">Add Hugot
                        <i class="material-icons right">send</i>
                    </a>
                    <h1>Report List</h1>

                    <div id="hugot-list">
                        <table class="responsive-table bordered striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Hugot Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- tr -->
                                @for($i=0; $i < count($hugot); $i++)
                                <tr>
                                    <td>{{($i+1)}}</td>
                                    <td>{{$hugot[$i]->hugot_message}}</td>
                                    <td>
                                        <a class="btn-floating blue" href="{{url('hugot-edit')}}/{{$hugot[$i]->hugot_id}}">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <a class="delete-btn btn-floating red" anchor-plate-id="{{$hugot[$i]->hugot_id}}">
                                            <i class="material-icons">delete</i>
                                        </a>
                                    </td>
                                </tr>
                                @endfor
                                <!-- tr -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@include('web.footer')