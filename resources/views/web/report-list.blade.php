
@include('web.head')
@extends('web.script-extend')
@section('import-page-js', 'report.js')

@include('web.nav')
<form action="{{url('delete-report')}}" method="GET" id="formDeleteReport">
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12">

                    <a class="btn waves-effect waves-light pull-right light-blue m-t-25" href="{{url('report-create')}}">Add Report
                        <i class="material-icons right">send</i>
                    </a>
                    <h1>Report List</h1>

                    <div id="hugot-list">
                        <table class="responsive-table bordered striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Plate No.</th>
                                    <th>Category</th>
                                    <th>Date and Time</th>
                                    <th>Remarks</th>
                                    <th>Location</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- tr -->
                                @for($i=0; $i < count($reports); $i++)
                                <tr>
                                    <td>{{($i+1)}}</td>
                                    <td>{{$reports[$i]->plate->plate_number}}</td>
                                    <td>{{$reports[$i]->category->category_name}}</td>
                                    <td>{{date('m/d/y  g:i A', strtotime($reports[$i]->report_timestamp))}}</td>
                                    <td>{{$reports[$i]->report_remarks}}</td>
                                    <td>{{$reports[$i]->report_location}}</td>
                                    <td>{{$reports[$i]->report_name}}</td>
                                    <td>
                                        <a class="delete-btn btn-floating red" anchor-plate-id="{{$reports[$i]->report_id}}">
                                            <i class="material-icons">delete</i>
                                        </a>
                                        <a class="attach-btn btn-floating orange modal-trigger" href="#attachment" title="View Attachment">
                                            <i class="material-icons">attach_file</i>
                                        </a>
                                    </td>
                                </tr>
                                
                                <div id="attachment" class="modal">
                                        <div class="modal-content">
                                            <img src="../../img/{{$reports[$i]->report_attachment}}" class="responsive-img">
                                        </div>
                                        <div class="modal-footer">
                                            <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                                        </div>
                                </div>
                                @endfor
                                <!-- tr -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@include('web.footer')