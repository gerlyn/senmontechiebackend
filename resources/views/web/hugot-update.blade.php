
@include('web.head')
@extends('web.script-extend')
@section('import-page-js', 'hugot.js')

@include('web.nav')
<form action="#" method="GET" id="formDeleteReport">
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12">
                    <a class="btn waves-effect waves-light pull-right grey darken-2 m-t-25" href="{{url('hugot-list')}}">
                        <i class="material-icons left">arrow_back</i>Back to Hugot List</a>
                    <h1>Update Hugot</h1>
                </div>
            </div>
            <form>
                <div class="row">
                    <form class="col s12">
                        <div class="input-field col s12">
                            <input id="hugot" type="text" class="validate" value="{{$hugot->hugot_message}}">
                            <label for="hugot">Modify your witty hugot message here:</label>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col s12">
                        <button id="save-hugot" class="btn waves-effect waves-light" type="submit" name="action">Save
                            <i class="material-icons right">save</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</form>


@include('web.footer')