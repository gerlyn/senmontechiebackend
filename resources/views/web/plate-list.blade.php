@include('web.head')
@extends('web.script-extend')
@section('import-page-js', 'plate.js')

<body>
    @include('web.nav')
    <form action="{{url('delete-plate')}}" method="GET" id="formDeletePlate">
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col s12">
                        <a class="btn waves-effect waves-light pull-right light-blue m-t-25" href="{{url('createPlateWeb')}}">Add Plate
                            <i class="material-icons right">send</i>
                        </a>
                        <h1>Plate List</h1>

                        <div id="hugot-list">
                        <form action="{{url('delete-plate')}}" method="POST">
                            <table class="responsive-table bordered striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Plate No.</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- tr -->
                                    @for($i = 0; $i < $plates->count() ; $i++)
                                    <tr>
                                        <td>{{($i+1)}}</td>
                                        <td>{{$plates[$i]->plate_number}}</td>
                                        <td>
                                            <a class="delete-btn btn-floating red" anchor-plate-id="{{$plates[$i]->plate_id}}">
                                                <i class="material-icons">delete</i>
                                            </a>    
                                        </td>
                                    </tr>
                                    @endfor
                                    <!-- tr -->
                                </tbody>
                            </table>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @include('web.footer')

    