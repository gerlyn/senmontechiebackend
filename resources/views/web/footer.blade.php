
<footer class="page-footer pastel-red">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h5 class="white-text">Women Who Code Manila Hackaton 2017</h5>
                <p class="grey-text text-lighten-4">Women Who Code is a global non-profit organization dedicated to inspiring women to succeed in technology
                    careers.
                </p>
            </div>
        </div>
    </div>
    <div class="footer-copyright brown">
        <div class="container pastel-red-text">
            &copy; SenMonTechie 2017
        </div>
    </div>
</footer>

</body>
</html>