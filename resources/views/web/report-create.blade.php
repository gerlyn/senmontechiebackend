
@include('web.head')
@include('web.nav')

    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12">
                    <a class="btn waves-effect waves-light pull-right grey darken-2 m-t-25" href="{{url('report-list')}}">
                        <i class="material-icons left">arrow_back</i>View Report List</a>
                    <h1>May isusumbong ka ba?</h1>
                </div>
            </div>
            <div class="row">
                @if(Session::has('message'))
                    {{Session::get('message')}}
                @endif
                <div id="repot" class="col s12">
                    <div id="accident-form">
                        <form action="{{url('createReportWeb')}}" method ="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <input id="Plate No." name="report_plate_number" type="text" class="validate" value="ABZ321">
                                    <label for="Plate No.">Plate No.</label>
                                    <span class="{{ $errors->has('report_plate_number') ? 'required' : 'hide' }}">*Required</span>
                                </div>
                                <div class="input-field col s12 m6 ">
                                    <select name="report_category_id" >
                                        <option value="0" disabled selected>Select Category</option>
                                        <option value="3">Accident</option>
                                        <option value="2">Complaint</option>
                                        <option value="1">Offense</option>
                                    </select>
                                    <label>Category</label>
                                    <span class="{{ $errors->has('report_category_id') ? 'required' : 'hide' }}">*Required</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="remarks" name= "report_remarks" type="text" class="validate" value="{{old('report_remarks')}}">
                                    <label for="remarks">Remarks</label>
                                    <span class="{{ $errors->has('report_remarks') ? 'required' : 'hide' }}">*Required</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="location" name= "report_location" type="text" class="validate" value="{{old('report_location')}}">
                                    <label for="location">Location</label>
                                    <span class="{{ $errors->has('report_location') ? 'required' : 'hide' }}">*Required</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <div class="file-field input-field">
                                        <div class="btn orange" >
                                            <span>Attachment</span>
                                            <input type="file" name="report_attachment">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <input id="first_name" name= "report_name" type="text" class="validate"  value="{{old('report_name')}}">
                                    <label for="first_name">Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <button id="submit-report" class="btn waves-effect waves-light">Report
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>                                    
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('web.footer')
