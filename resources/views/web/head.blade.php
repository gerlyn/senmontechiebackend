<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8">
	<meta name="format-detection" content="telephone=no">
	<meta property="og:url"           content="http://highwaypatatas.tk" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="HighwayPatatas" />
	<meta property="og:description"   content="SenmonTechie Entry - Women Who Code Manila Hackaton 2017
Women Who Code is a global non-profit organization dedicated to inspiring women to succeed in technology careers." />
	<meta property="og:image"         content="../../img/resized_highway-patatas.png" />

    <title>SenMonTechie - Highway Patatas</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="../../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link href="../../css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <!--  Scripts-->
    <script type="text/javascript" src="../../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/materialize.min.js"></script>
    <script type="text/javascript" src="../../js/sweetalert.min.js"></script>
    <script type="text/javascript" src="../../js/init.js"></script>

    <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!}
    </script>
</head>