<nav class="pastel-teal" role="navigation">
        <div class="nav-wrapper container">
            <a href="/">
                <img src="../../img/highway-patatas-text.png" class="responsive-img m-t-15 m-b-15">
            </a>
            <ul class="right hide-on-med-and-down">
                <!-- <li>
                    <a href="/user-list.html">Admin</a>
                </li> -->
                <li>
                    <a href="{{url('hugot-list')}}">Hugot</a>
                </li>
                <li>
                    <a href="{{url('plate-list')}}">Plate</a>
                </li>
                <li>
                    <a class="waves-effect waves-light btn pastel-red" href="{{url('report-create')}}">Report!</a>
                </li>
            </ul>

            <ul id="nav-mobile" class="side-nav">
                <!-- <li>
                    <a href="/user-list.html">Admin</a>
                </li> -->
                <li>
                    <a href="{{url('hugot-list')}}">Hugot</a>
                </li>
                <li>
                    <a href="{{url('plate-list')}}">Plate</a>
                </li>
                <li>
                    <a class="waves-effect waves-light btn pastel-red" href="/">Report!</a>
                </li>
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse">
                <i class="material-icons">menu</i>
            </a>
        </div>
    </nav>