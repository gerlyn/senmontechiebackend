
@include('web.head')
@extends('web.script-extend')
@section('import-page-js', 'report.js')

@include('web.nav')
<form action="#" method="GET" id="formDeleteReport">
<div class="container">
<div class="section">
    <div class="row">
        <div class="col s12">
            <a class="btn waves-effect waves-light pull-right grey darken-2 m-t-25" href="/plate-list.html">
                <i class="material-icons left">arrow_back</i>Back to Plate List</a>
    
            <h1>Add Plate</h1>
        </div>
    </div>
    
    <form>
        <div class="row">
            <div class="input-field col s12 m6">
                <input id="Plate No." type="text" class="validate">
                <label for="Plate No.">Plate No.</label>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <button id="save-plate" class="btn waves-effect waves-light" type="submit" name="action">Add
                    <i class="material-icons right">send</i>
                </button>
            </div>                        
        </div>                    
    </form>
</div>
</div>

</form>


@include('web.footer')