/*
SQLyog Community v12.3.2 (64 bit)
MySQL - 10.1.22-MariaDB : Database - huli_ka_ba
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`huli_ka_ba` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `huli_ka_ba`;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(15) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `categories` */

insert  into `categories`(`category_id`,`category_name`) values 
(1,'Offense'),
(2,'Complaint'),
(3,'Accident');

/*Table structure for table `hugots` */

DROP TABLE IF EXISTS `hugots`;

CREATE TABLE `hugots` (
  `hugot_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hugot_message` varchar(255) NOT NULL,
  PRIMARY KEY (`hugot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `hugots` */

insert  into `hugots`(`hugot_id`,`hugot_message`) values 
(1,'Wala pa. Pa-cheese burger ka naman!'),
(2,'Wala. tulad ng pag-asa mong manalo sa lotto.'),
(3,'Hmm. Wala naman malay mo bukas meron na!');

/*Table structure for table `plates` */

DROP TABLE IF EXISTS `plates`;

CREATE TABLE `plates` (
  `plate_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plate_number` varchar(7) NOT NULL,
  PRIMARY KEY (`plate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `plates` */

insert  into `plates`(`plate_id`,`plate_number`) values 
(1,'ABZ321'),
(2,'AEW831'),
(3,'KHJ983');

/*Table structure for table `reports` */

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_plate_id` int(10) unsigned NOT NULL,
  `report_category_id` int(10) unsigned NOT NULL,
  `report_remarks` varchar(255) NOT NULL,
  `report_user_name` varchar(30) DEFAULT NULL,
  `report_attachment` varchar(30) DEFAULT NULL,
  `report_location` varchar(30) DEFAULT NULL,
  `report_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`report_id`),
  KEY `report_plate_id` (`report_plate_id`),
  KEY `report_category_id` (`report_category_id`),
  CONSTRAINT `report_category_id` FOREIGN KEY (`report_category_id`) REFERENCES `categories` (`category_id`),
  CONSTRAINT `report_plate_id` FOREIGN KEY (`report_plate_id`) REFERENCES `plates` (`plate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `reports` */

insert  into `reports`(`report_id`,`report_plate_id`,`report_category_id`,`report_remarks`,`report_user_name`,`report_attachment`,`report_location`,`report_timestamp`) values 
(1,1,1,'Illegal Overtaking','Josefa Manalo',NULL,'Makati','2017-11-24 20:50:01'),
(2,1,2,'Rude Driver',NULL,NULL,'Taguig','2017-11-24 20:52:37'),
(4,1,2,'Obscene Driver',NULL,NULL,'Quezon City','2017-11-24 20:53:29'),
(5,3,3,'Nadaganan ng mixer.','Juan Jose',NULL,'Rizal','2017-11-24 20:54:20');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
