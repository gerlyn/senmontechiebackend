<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('','ReportController@index');
Route::post('','ReportController@index');
Route::get('report-create','ReportController@reportCreate');

Route::get('plate-list','PlateNumberController@plateList');
Route::get('delete-plate/{plateId}','PlateNumberController@deletePlate');
Route::get('report-list','ReportController@getReports');
Route::get('delete-report/{reportId}','ReportController@deleteReport');
// Route::get('hugot-add','HugotController@hugotAdd');
Route::get('hugot-edit/{hugotId}','HugotController@hugotEdit');
Route::get('hugot-list','HugotController@hugotList');
Route::get('delete-hugot/{hugotId}','HugotController@deleteHugot');

Route::get('findPlateNumber','PlateNumberController@find');
Route::post('createReport','ReportController@createReport');
Route::post('createReportWeb','ReportController@createReportWeb');
Route::post('createPlateWeb','PlateNumberController@renderCreatePlate');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**Protected routes need login admin */
Route::middleware(['auth'])->group(function () {
    Route::get('plate', 'PlateController@renderPlate');

});
