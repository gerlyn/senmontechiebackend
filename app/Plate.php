<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @author Gerlyn Bauyan <gbauyan@gmail.com>
 * @summary This is Plate's model
 * @since 11-24-2017
 */
class Plate extends Model
{
    //
    protected $table = 'plates';
    public $timestamps = false;
    protected $primaryKey = 'plate_id';
    protected $fillable = [
        'plate_number'
    ];

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary One Plate Number may have Many Reports
     * @return object Report
     * @since 11-24-2017
     */
    public function plate_reports()
    {
        return $this->hasMany('App\Report', 'report_plate_id', 'plate_id');
    }

}
