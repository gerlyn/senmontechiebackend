<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @author Gerlyn Bauyan <gbauyan@gmail.com>
 * @summary This is Category's model
 * @since 11-24-2017
 */
class Category extends Model
{
    //
    protected $table = 'categories';
    public $timestamps = false;
    protected $primaryKey = 'category_id';
    protected $fillable = [
        'category_name'
    ];



}
