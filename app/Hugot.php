<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @author Gerlyn Bauyan <gbauyan@gmail.com>
 * @summary This is Hugot's model
 * @since 11-24-2017
 */
class Hugot extends Model
{
    //
    protected $table = 'hugots';
    public $timestamps = false;
    protected $primaryKey = 'hugot_id';
    protected $fillable = [
        'hugot_message'
    ];

}
