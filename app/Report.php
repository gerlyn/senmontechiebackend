<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @author Gerlyn Bauyan <gbauyan@gmail.com>
 * @summary This is Report's model
 * @since 11-24-2017
 */
class Report extends Model
{
    //
    protected $table = 'reports';
    public $timestamps = false;
    protected $primaryKey = 'report_id';
    protected $fillable = [
        'report_plate_id',
        'report_category_id',
        'report_remarks',
        'report_user_name',
        'report_attachment',
        'report_location',
        'report_timestamp',
    ];


    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary Get reports by report_category_id 
     * @return object reports with respect to plate_id && category
     * @since 11-24-2017
     */
    public static function getByCategory($plate_result, $report_category_id)
    {
        return $plate_result
                    ->plate_reports
                    ->where('report_category_id', $report_category_id);
    }

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary Create new entry to reports
     * @return void
     * @since 11-24-2017
     */
    public static function create($request)
    {
        $newReport = new Report;
        $newReport->report_plate_id = $request->report_plate_id;
        $newReport->report_category_id = $request->report_category_id;
        $newReport->report_remarks = $request->report_remarks;
        $newReport->report_name = $request->report_name;
        $newReport->report_attachment = (!empty($request->file('report_attachment')))
                                            ? basename($request->file('report_attachment')->store('/','storage-upload'))
                                            : NULL;
        $newReport->report_location = $request->report_location;
        $newReport->save();
    }

     /**
     * @author Gerlyn Bauyan <gbauyan@gmail.com>
     * @summary Create new entry to reports from web
     * @return back()
     * @since 11-24-2017
     */
    public static function createFromWeb($request,$plate)
    {
        $newReport = new Report;
        $newReport->report_plate_id = $plate->plate_id;
        $newReport->report_category_id = $request->report_category_id;
        $newReport->report_remarks = $request->report_remarks;
        $newReport->report_name = $request->report_name;
        $newReport->report_attachment = (!empty($request->file('report_attachment')))
                                            ? basename($request->file('report_attachment')->store('/','storage-upload'))
                                            : NULL;
        $newReport->report_location = $request->report_location;
        $newReport->save();

        $message = "You have successfully submitted a report!";
        return back()
                ->with('message', $message);
    }

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary report relationship belongs to plate
     * @return object Plate
     * @since 11-25-2017
     */
    public function plate()
    {
        return $this->belongsTo('App\Plate','report_plate_id','plate_id');
    }

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary report relationship belongs to category
     * @return object Plate
     * @since 11-25-2017
     */
    public function category()
    {
        return $this->belongsTo('App\Category','report_category_id','category_id');
    }

}
