<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Plate;
use App\Report;
use App\Hugot;

class HugotController extends Controller
{
    
    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary delete a hugot number by id
     * @return view hugot list
     * @since 11-25-2017
     */
    public function hugotList()
    {
        $hugot = Hugot::get();
        return view('web.hugot-list')
                ->with('hugot',$hugot);
    }
  /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary delete a hugot number by id
     * @return view hugot list
     * @since 11-25-2017
     */
    public function deleteHugot($hugotId)
    {
        $hugot = Hugot::find($hugotId);
        $hugot->delete();
    }

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary delete a hugot number by id
     * @return view hugot list
     * @since 11-25-2017
     */
    public function hugotEdit($hugotId)
    {
        $hugot = Hugot::find($hugotId);
        return view('web.hugot-update')
                ->with('hugot',$hugot);
    }

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary delete a hugot number by id
     * @return view hugot list
     * @since 11-25-2017
     */
    public function hugotUpdate($hugotId)
    {
        $hugot = Hugot::find($hugotId);
        return back();
    }
    
}
