<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PlateNumberController;
use App\Plate;
use App\Report;

class ReportController extends Controller
{
   /**
    * @author Karen Cano <karenireneccano@gmail.com>
    * @summary Create new entry to reports
    * @return void
    * @since 11-24-2017
    */
   public function createReport(Request $request)
   {
      return Report::create($request);
   }

    /**
     * @author Gerlyn Bauyan <gbauyan@gmail.com>
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary createReportWeb
     * @return back()
     * @since 11-24-2017
     */
   public function createReportWeb(Request $request)
   {
      $validator = $request->validate([
        'report_plate_id'    => 'required',
        'report_category_id' => 'required',
        'report_remarks'     => 'required',
        'report_location'    => 'required'
      ]);

      if (!($validator)) {
          return redirect()->back()->withErrors($validator)->withInput();
      }
      $plate = Plate::where('plate_number',$request->report_plate_number)
                            ->first();
      return Report::createFromWeb($request,$plate);
   }

   /**
   * @author Gerlyn Bauyan <gbauyan@gmail.com>
   * @summary index
   * @return 'web.index'
   * @since 11-24-2017
   */
   public function index(Request $request)
   {
       $data = [];
       if($request->isMethod('get'))
        {
            return view('web.index')
            ->with('data',$data);
        }
       else
        {
            $plateNumberWeb = new PlateNumberController();
            return $plateNumberWeb->findPlateNumberWeb($request);
        }
   }

   /**
   * @author Karen Cano <karenireneccano@gmail.com>
   * @summary getReports
   * @return 'web.report-list'
   * @since 11-25-2017
   */
   public function getReports()
   {
      $reports = Report::get();
      return view('web.report-list')
            ->with('reports', $reports); 
   }

  /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary delete a report number by id
     * @return view report list
     * @since 11-25-2017
     */
    public function deleteReport($reportId)
    {
        $report = Report::find($reportId);
        $report->delete();
    }

    //render report create
    public function reportCreate()
    {
        return view('web.report-create');
    }
}
