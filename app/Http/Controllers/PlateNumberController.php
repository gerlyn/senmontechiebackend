<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Plate;
use App\Report;
use App\Hugot;

class PlateNumberController extends Controller
{
    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @author Gerlyn Bauyan <gbauyan@gmail.com>
     * @summary Find existence of plate number
     * @return object reports
     * @since 11-24-2017
     */

    public function createPlateWeb(Request $request)
    {
       $validator = $request->validate([
         'plate_number' => 'required'
       ]);
 
       if (!($validator)) {
           return redirect()->back()->withErrors($validator)->withInput();
       }
        $new_plate = new Plate;
        $new_plate->plate_number = $request->plate_number;
        $new_plate->save();
    }

    public function find(Request $request)
    {
        $plate_number = $request->plate_number;
        $plate_result = Plate::where('plate_number',$plate_number)
                                ->first();
        if(is_null($plate_result))
        {
            return $this->getRandomMessage();
        }
        else
        {
            $report_cnt = $plate_result->plate_reports->count();
            if($report_cnt   == 0){
                return $this->getRandomMessage();
            }  
            else{
                return $this->getReports($plate_result);
            }
        }
    }

    public function index()
    {
        return view('web.index');
    }

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary Get reports respective of the plate number found
     * @return object reports
     * @since 11-24-2017
     */
    public function getReports($plate_result)
    {
        $result = array();
        $result["offenses"] = array();
        $offenses = Report::getByCategory($plate_result, 1);
        /**offenses */
        foreach($offenses as $offense)
        {
            array_push($result["offenses"],$offense);
        }
        /**complaints */
        $result["complaints"] = array();
        $complaints = Report::getByCategory($plate_result, 2);
        
        foreach($complaints as $complaint)
        {
            array_push($result["complaints"],$complaint);
        }
        /**accidents */
        $result["accidents"] = array();
        $accidents = Report::getByCategory($plate_result, 3);
        foreach($accidents as $accident)
        {
            array_push($result["accidents"],$accident);
        }  
        $result["message"] = array();
        
        return $result;
    }//end of getReports()

    public function getReports2($plate_result)
    {
        $result = array();
        $result["offenses"] = array();
        $offenses = Report::getByCategory($plate_result, 1);
        /**offenses */
        foreach($offenses as $offense)
        {
            array_push($result["offenses"],$offense);
        }
        /**complaints */
        $result["complaints"] = array();
        $complaints = Report::getByCategory($plate_result, 2);
        
        foreach($complaints as $complaint)
        {
            array_push($result["complaints"],$complaint);
        }
        /**accidents */
        $result["accidents"] = array();
        $accidents = Report::getByCategory($plate_result, 3);
        foreach($accidents as $accident)
        {
            array_push($result["accidents"],$accident);
        }  
        //$result["message"] = array();
        
        return $result;
    }//end of getReports()

  
    /**
     * @author Gerlyn Bauyan <gbauyan@gmail.com>
     * @summary Get reports respective of the plate number found
     * @return object reports
     * @since 11-25-2017
     */
    public function getRandomMessage()
    {

        $hugots = Hugot::pluck('hugot_message');
        // dd($hugots);
        $hugot_rand = rand(0,($hugots->count() - 1));
        $hugot = $hugots[$hugot_rand];
        // dd($hugot);
        $result["offenses"] = array();
        $result["complaints"] =  array();
        $result["accidents"] =  array();
        $result["message"] = $hugot;
        
        return $result;
    }

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary Plate list and view
     * @return view plate list
     * @since 11-24-2017
     */
    public function plateList()
    {
        $plates = Plate::get();
        return view('web.plate-list')
                ->with('plates',$plates);
    }

    /**
     * @author Karen Cano <karenireneccano@gmail.com>
     * @summary delete a plate number by id
     * @return view plate list
     * @since 11-25-2017
     */
    public function deletePlate($plateId)
    {
        $plate = Plate::find($plateId);
        $plate->delete();
    }

    
    public function renderCreatePlate()
    {
        return view('web.plate-create');
    }

    public function findPlateNumberWeb(Request $request)
    {
        $validator = $request->validate([
            'plate_number'    => 'required'
          ]);
    
          if (!($validator)) {
              return redirect()->back()->withErrors($validator)->withInput();
          }


        $plate_number = $request->plate_number;
        $plate_result = Plate::where('plate_number',$plate_number)
                                ->first();
        if(is_null($plate_result))
        {
            $data = $this->getRandomMessage();
            return view('web.index')
                ->with('data',json_encode($data));
                
        }
        else
        {
            $report_cnt = $plate_result->plate_reports->count();
            if($report_cnt   == 0){
                // return $this->getRandomMessage();
                $data = $this->getRandomMessage();
                // return view('web.index')
                return view('web.index')
                ->with('data',json_encode($data));
            }  
            else{
                $data = $this->getRandomMessage();
                return view('web.index')
                ->with('data',json_encode($data));
                
            }
        }
    }
}


